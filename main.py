from PIL import Image

from src.image_processor import ImageProcessor

image = Image.open("test_data/test.jpg")
processor = ImageProcessor()
keywords = processor.process(image)

print(keywords)