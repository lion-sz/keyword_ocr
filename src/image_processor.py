from PIL import Image
from tesserocr import PyTessBaseAPI, RIL, iterate_level
from typing import List
from sklearn.cluster import KMeans
import numpy as np

from src.line import Line


class ImageProcessor:
    """
    This class provides methods to process an image and return the text.

    To do so I first call tesseract on the entire file, processing it line by line.
    I then separate the lines into two pages based on whether the line starts
    to the left or the right of the image's center.
    I remove all empty lines and the first line on each page.
    I use KNearest clustering on the line's x-coordinates to differentiate  between
    keywords and explanation.

    The only method that is supposed to be called by external classes is :func:`~ImageProcessor.process`.
    """

    def process(self, image: Image.Image) -> List[str]:
        """
        This method is supposed to be called with the image.
        It manages the internal methods to actually process image and
        returns a list of all keywords contained in the image.

        :param image: The image that should be processed.
        :type image: :class:`Image.Image`
        :return: A list of keywords.
        :rtype: list(str)
        """

        # run Tesseract
        lines = self.extract_lines(image)
        # strip not needed lines
        lines = self.remove_not_needed(lines)
        # find the keywords
        keywords_p0 = self.find_keywords(lines, 0)
        keywords_p1 = self.find_keywords(lines, 1)
        return keywords_p0 + keywords_p1

    @staticmethod
    def extract_lines(image: Image.Image) -> List[Line]:
        """
        This function extracts the text lines from the picture
        into a list of :class:`Line`.

        :param image: The image that should be parsed
        :type image: :class:`Pil.Image.Image`
        :return: A list of the lines in, with their x coordinate.
        :rtype: list(:class:`Line`)
        """
        level = RIL.TEXTLINE
        return_list = []

        img_width = image.size[0]

        with PyTessBaseAPI() as api:
            api.SetImage(image)
            api.Recognize()
            ri = api.GetIterator()
            for r in iterate_level(ri, level):
                word = r.GetUTF8Text(level)
                box = r.BoundingBox(level)
                return_list.append(Line.from_coords(word, img_width, box))

        return return_list

    @staticmethod
    def remove_not_needed(lines: List[Line]) -> List[Line]:
        """
        This method removes the not needed lines from the lines list.
        It first removes all empty lines and all lines with only one character.
        Furthermore, I remove the first line of each page.

        :param lines: The lines list returned from the :func:`~ImageProcessor.extract_lines`.
        :type lines: list(:class:`Line`)
        :return: A cleaned list of lines.
        :rtype: list(:class:`Line`)
        """

        # first I remove all empty lines.
        lines = [line for line in lines if len(line.text) > 1]
        # and now the first line of each page. For the first page this is line one.
        del lines[0]
        tmp = min([i for i in range(0, len(lines)) if lines[i].page == 1])
        del lines[tmp]

        return lines

    @staticmethod
    def find_keywords(lines: List[Line], page: int) -> List[str]:
        """
        This method extracts the lines that contain a keyword for a given page.

        To do so I first extract all lines in the given page.
        I then calculate a K-Nearest cluster with 2 clusters and
        all observations belonging to the smaller cluster are keywords.

        :param lines: The lines List
        :type lines: list(:class:`Line`)
        :param page: The page that should be processed.
        :param page: int
        :return: A list of the keywords in the line.
        :rtype: list(str)
        """

        lines = [line for line in lines if line.page == page]
        dat = np.array([line.x for line in lines])
        kmeans = KMeans(n_clusters=2)
        kmeans.fit(dat.reshape(-1, 1))

        # now I need to extract all observations that belong to the smaller cluster.
        smaller_cluster = 0 if kmeans.cluster_centers_[0] < kmeans.cluster_centers_[1] else 1
        result = [lines[i] for i in range(0, len(lines))
                  if kmeans.labels_[i] == smaller_cluster]
        # and extract the keyword. The separator character is somewhat special, unicode value 8212.
        return [line.text.split('—')[0] for line in result]
