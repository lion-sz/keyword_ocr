from __future__ import annotations
# this is used to allow the classmethod to contain the typehint line.
from typing import NamedTuple, Tuple


class Line(NamedTuple):
    """
    This Tuple provides a way to store the information for a line of ocr text.
    It stores not only the text, but also the page information and the bounding box.
    """

    text: str
    """This is the text extracted by the tesseract ocr."""
    page: int
    """The information if the line is on the left (0) or right side (1)."""
    x: int
    """The most left x-coordinate of the bounding box."""
    y: int
    """The lower most y-coordinate of the bounding box."""
    w: int
    """The width of the bounding box."""
    h: int
    """The height of the bounding box."""

    @classmethod
    def from_coords(cls, text: str, img_width: int,
                    coords: Tuple[int, int, int, int]) -> Line:
        """
        This classmethod provides a constructor that allows me to give the coordinate tuple.

        :param text: The text of the line.
        :type text: str
        :param img_width: The width of the original image in pixels.
            This is used to determine the page that the line is found on.
        :type img_width: int
        :param coords: The coordinates of the bounding box returned by tesseract.
        :type coords: tuple(int, int, int, int)
        :return: An :class:`Line` object storing this information.
        :rtype: :class:`Line`
        """
        return cls(text.strip(), 0 if coords[0] < img_width / 2 else 1,
                   coords[0], coords[1], coords[2], coords[3])
