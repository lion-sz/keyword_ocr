.. Keyword Detection documentation master file, created by
   sphinx-quickstart on Sun Sep 20 18:55:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Keyword Detection's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   API Reference <reference.rst>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
